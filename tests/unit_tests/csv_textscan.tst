//
//  Copyright (C) 2010 - 2011 - DIGITEO - Allan CORNET
//  Copyright (C) 2011 - DIGITEO - Michael Baudin
//
//  This file must be used under the terms of the CeCILL.
//  This source file is licensed as described in the file COPYING, which
//  you should have received as part of this distribution.  The terms
//  are also available at
//  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//


// <-- JVM NOT MANDATORY -->

// =============================================================================
A = "        1;        2;     3";
B = "        4;        5;     6";
C = [A;B];
bbSTR = csv_textscan(C, ';', [], "string");
expected = [
"        1" , "        2" , "     3" 
"        4" , "        5" , "     6" 
];
assert_checkequal ( bbSTR , expected );
// =============================================================================
bbDouble = csv_textscan(C, ';', [], 'double');
expected = [1 2 3;4 5 6];
assert_checkequal ( bbDouble , expected );
// =============================================================================
path = fullfile(csv_getToolboxPath(),"tests","unit_tests");
// =============================================================================
Mstr = [
"1" "0" "0" "0" "0"
"0" "1" "0" "0" "0"
"0" "0" "1" "0" "0"
];
//
r = mgetl(fullfile(path,"M_1.csv"));
b = csv_textscan(r, [], [], "string");
assert_checkequal (b,Mstr);
//
r = mgetl(fullfile(path,"M_2.csv"));
b = csv_textscan(r, ascii(9), [], "string");
assert_checkequal (b,Mstr);
//
r = mgetl(fullfile(path,"M_3.csv"));
b = csv_textscan(r, " ", ",", "string");
assert_checkequal (b,Mstr);
//
r = mgetl(fullfile(path,"M_4.csv"));
b = csv_textscan(r, ";", ",", "string");
assert_checkequal (b,Mstr);
// =============================================================================
Nstr = [
"Nan"   "1"    "Nan"
"-Inf"  "Inf"  "4"
];
//
r = mgetl(fullfile(path,"N_1.csv"));
b = csv_textscan(r, [], [], "string");
assert_checkequal (b,Nstr);
//
r = mgetl(fullfile(path,"N_2.csv"));
b = csv_textscan(r, ascii(9), [], "string");
assert_checkequal (b,Nstr);
//
r = mgetl(fullfile(path,"N_3.csv"));
b = csv_textscan(r, " ", ",", "string");
assert_checkequal (b,Nstr);
//
r = mgetl(fullfile(path,"N_4.csv"));
b = csv_textscan(r, ";", ",", "string");
assert_checkequal (b,Nstr);
// =============================================================================
Kstr = [
"1.1000000000000001" "0.10000000000000001"
"0.10000000000000001" "1.1000000000000001"
"0.10000000000000001" "0.10000000000000001"
];
Kstr2 = [
"1,1000000000000001" "0,10000000000000001"
"0,10000000000000001" "1,1000000000000001"
"0,10000000000000001" "0,10000000000000001"
];
//
r = mgetl(fullfile(path,"K_1.csv"));
b = csv_textscan(r, [], [], "string");
assert_checkequal (b,Kstr);
//
r = mgetl(fullfile(path,"K_2.csv"));
b = csv_textscan(r, ascii(9), [], "string");
assert_checkequal (b,Kstr);
//
r = mgetl(fullfile(path,"K_3.csv"));
b = csv_textscan(r, " ", ",", "string");
assert_checkequal (b,Kstr2);
//
r = mgetl(fullfile(path,"K_4.csv"));
b = csv_textscan(r, ";", ",", "string");
assert_checkequal (b,Kstr2);
// =============================================================================
S = [
  "Allan",                  "2", "CORNET";
  "csv read/write toolbox", "3", "for scilab"
];
//
r = mgetl(fullfile(path,"S_1.csv"));
b = csv_textscan(r, "|", [], "string");
assert_checkequal (b,S);
//
r = mgetl(fullfile(path,"S_2.csv"));
b = csv_textscan(r, ascii(9), [], "string");
assert_checkequal (b,S);
//
r = mgetl(fullfile(path,"S_3.csv"));
b = csv_textscan(r, "!", ",", "string");
assert_checkequal (b,S);
//
r = mgetl(fullfile(path,"S_4.csv"));
b = csv_textscan(r, ";", ",", "string");
assert_checkequal (b,S);
//
r = mgetl(fullfile(path,"S_1.csv"));
b = csv_textscan(r, "|", ".", "string");
assert_checkequal (b,S);
//
r = mgetl(fullfile(path,"S_1.csv"));
b = csv_textscan(r, "|", ".", "double");
ref = [%nan , 2, %nan; %nan, 3, %nan];
assert_checkequal ( b , ref);
// =============================================================================

