/*
*  Copyright (C) 2010 - 2011 - DIGITEO - Allan CORNET
*
*  This file must be used under the terms of the CeCILL.
*  This source file is licensed as described in the file COPYING, which
*  you should have received as part of this distribution.  The terms
*  are also available at
*  http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
*
*/

#ifndef __CHECKCSVWRITEFORMAT_H__
#define __CHECKCSVWRITEFORMAT_H__

int checkCsvWriteFormat(const char *format);

#endif /* __CHECKCSVWRITEFORMAT_H__ */
/* ========================================================================== */
